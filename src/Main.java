import java.util.Scanner;

public class Main {
    public static void main(String[] args)
    {
        System.out.println("Hello world!");



//        Урок 6. Ключевое слово final
//        Цель задания:
//        Знакомство с модификатором final
//    Задание:
//        1.
//        Создайте final массив чисел. Увеличьте каждый его элемент вдвое.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
 final int[] arr1 = new int[]{1,2,3,4,5,6,7,8,9,10};
 for (int i=0;i<10;i++){
     arr1[i]+=2;
     System.out.println(arr1[i]);
 }



        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        2.
//        Найдите наибольшее число из трех, используя только final
//        -
//                переменные.
final int x2=2,y2=3,z2=4;
        if (x2>y2&&x2>z2) System.out.println("x2 это maximum");
        if (y2>x2&&y2>z2) System.out.println("y2 это maximum");
        if (z2>y2&&z2>y2) System.out.println("z2 это maximum");

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
 //        3.
//        Измените final переменную. Получилось?

        System.out.println("не получилось");

//        4.
//        Пользователь вводит слово, подсчитайте количество уникальных букв в
//        этом слове. (Повторяющиеся считаем за одну, в слове окно
//        -
//                три
//        уникальные буквы, окн). Используйте только final
//        -
//                переменные. Подсказка:
//        используйте ма
//        ссив.
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        final Scanner scanner = new Scanner(System.in);
        System.out.println("Введите слово: ");
        final String str3 = scanner.nextLine();
        final char ch3[] = new char[str3.length()];
        for (int i=0;i<str3.length();i++){
            ch3[i]='0';
            if (!chkArr(ch3,str3.charAt(i))) {
            ch3[i]=str3.charAt(i);
            }
        }
        System.out.println("всего уникальных букв, столько, сколько строк: ");
        lenArr(ch3);


//                Критерии оценивания:
//    1 балл
//            -
//            создан новый проект в IDE
//        2 балла
//            -
//            написана общая структура программы
//        3 балла
//            -
//            выполнено более 60% заданий, имеется не более 5 критичных
//            замечаний
//        4 балла
//            -
//            выполнено корректно более 80% технических заданий
//        5 баллов
//            -
//            все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов
//


    }

//проверка наличия символа в массиве
    //надеюсь в цикле i не должно быть тоже final...
    static final public boolean chkArr(char arr[], char ch){
        for (int i=0;i<arr.length;i++){
            if (arr[i]==ch) return true;
        }
        return false;
    }

    //считаем количество уникальных символов в массиве
    static final public void lenArr(char arr[]){
        for (int i=0;i<arr.length;i++){
            if (arr[i]!='0') System.out.println("уникальная буква");;
        }
    }



}